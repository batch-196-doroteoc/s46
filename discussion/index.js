console.log("CAMI!!!")

//mock database
let posts = [];

//count variable that will serve as the post id
let count = 1;

//Add post data

document.querySelector("#form-add-post").addEventListener("submit",(e)=>{
	//to prevent the page from loading, hinders the default behavior of the event
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	count++;
	showPosts(posts);
	alert("Movie Succesfully Added!");
	// console.log(posts);
});

//Show posts

const showPosts = (posts) => {
	console.log(posts);
	//sasaluhin nya lahat kada post
	let postEntries = '';

	posts.forEach((post)=>{
		console.log(post);
		postEntries += `
		<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>

		</div>
		`
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

//Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};

// Update a Post
document.querySelector("#form-edit-post").addEventListener('submit',(e)=>{

	e.preventDefault();

	for(let i = 0; i <posts.length; i++){

		if(posts[i].id.toString()===document.querySelector('#txt-edit-id').value){

			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Movie post succesfully updated!');

			break;
		}
	}
})

//Delete Post
// const deletePost = (id) => {

// 	document.querySelector("#div-post-entries").addEventListener('submit',(e)=>{

// 	e.preventDefault();

// 	for(let i = 0; i <posts.length; i++){

// 		if(posts[i].id.toString()===document.querySelector('#txt-edit-id').value){
// 			posts.remove()

// 			showPosts(posts);
// 			alert('Movie post succesfully deleted!');

// 			}
// 		}
// 	})
// }

// const deletePost = (id) => {

// 	document.querySelector("#div-post-entries").addEventListener('submit',(e)=>{

// 			e.preventDefault();
// 			posts.pop()

// 			showPosts(posts);
// 			alert('Movie post succesfully deleted!');

// 			})
// 		}


// const editPost = (id) => {
// 	let title = document.querySelector(`#post-title-${id}`).innerHTML;
// 	let body = document.querySelector(`#post-body-${id}`).innerHTML;

// 	document.querySelector('#txt-edit-id').value = id;
// 	document.querySelector('#txt-edit-title').value = title;
// 	document.querySelector('#txt-edit-body').value = body;
// };

// const deletePost = (id) => {
// 	for(let i = 0; i <posts.length; i++){

// 		if(posts[i].id.toString()===post.id){
// 			posts.splice(i,1);
// 		}
// 	}
// 	showPosts(posts);
// 	alert('Movie post is deleted!');
// };

///Activity 46
const deletePost = (id) => {
	for(let i = 0; i <posts.length; i++){

		if(posts[i].id.toString()===id){
			posts.splice(i,1);
		}
	}
	showPosts(posts);
	alert('Movie post is deleted!');
};